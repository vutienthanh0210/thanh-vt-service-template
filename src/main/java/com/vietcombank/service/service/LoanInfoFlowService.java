package com.vietcombank.service.service;

import com.vietcombank.service.entity.LoanInfoFlowEntity;
import com.vietcombank.service.repository.LoanInfoFlowEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class LoanInfoFlowService {
    @Autowired
    private LoanInfoFlowEntityRepository loanInfoFlowEntityRepository;

    public List<LoanInfoFlowEntity> retrieveAll() {
        return loanInfoFlowEntityRepository.findAll();
    }


    public LoanInfoFlowEntity getLoanFlowById(int id)
    {
        return loanInfoFlowEntityRepository.findById(id).get();
    }
    //saving a specific record by using the method save() of CrudRepository
    public void saveOrUpdate(LoanInfoFlowEntity LoanInfoFlowEntity)
    {
        loanInfoFlowEntityRepository.save(LoanInfoFlowEntity);
    }
    //deleting a specific record by using the method deleteById() of CrudRepository
    public void delete(int id)
    {
        loanInfoFlowEntityRepository.deleteById(id);
    }
    //updating a record
    public void update(LoanInfoFlowEntity LoanInfoFlowEntity, int loanid)
    {
        loanInfoFlowEntityRepository.save(LoanInfoFlowEntity);
    }
}
