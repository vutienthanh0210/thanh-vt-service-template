package com.vietcombank.service.service;

import com.vietcombank.service.entity.LoanInfoEntity;
import com.vietcombank.service.repository.LoanInfoEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class LoanInfoService {
    @Autowired
    private LoanInfoEntityRepository loanInfoEntityRepository;

    public List<LoanInfoEntity> retrieveAll() {
        return loanInfoEntityRepository.findAll();
    }


    public LoanInfoEntity getLoanById(int id)
    {
        return loanInfoEntityRepository.findById(id).get();
    }
    //saving a specific record by using the method save() of CrudRepository
    public void saveOrUpdate(LoanInfoEntity loanInfoEntity)
    {
        loanInfoEntityRepository.save(loanInfoEntity);
    }
    //deleting a specific record by using the method deleteById() of CrudRepository
    public void delete(int id)
    {
        loanInfoEntityRepository.deleteById(id);
    }
    //updating a record
    public void update(LoanInfoEntity loanInfoEntity, int loanid)
    {
        loanInfoEntityRepository.save(loanInfoEntity);
    }
}
