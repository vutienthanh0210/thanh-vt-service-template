package com.vietcombank.service.repository;

import com.vietcombank.service.entity.LoanInfoEntity;
import com.vietcombank.service.entity.LoanInfoFlowEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoanInfoFlowEntityRepository extends JpaRepository<LoanInfoFlowEntity, Integer> {

}