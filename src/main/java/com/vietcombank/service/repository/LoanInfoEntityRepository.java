package com.vietcombank.service.repository;

import com.vietcombank.service.entity.LoanInfoEntity;
import com.vietcombank.service.entity.SampleTableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface LoanInfoEntityRepository extends JpaRepository<LoanInfoEntity, Integer> {


//    List<Object> findById(int id);
}