package com.vietcombank.service.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.UUID;

@Entity
@Table(name = "LOAN_INFO_FLOW")
public class LoanInfoFlowEntity {

    @Getter
    @Setter
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int ID;

    @Getter
    @Setter
    @Column(name = "LOAN_ID", nullable = false)
    private int LOAN_ID;

    @Getter
    @Setter
    @Column(name = "STATUS")
    private String STATUS;

    @Getter
    @Setter
    @Column(name = "MODIFY_USER")
    private String MODIFY_USER;

    @Getter
    @Setter
    @Column(name = "MODIFY_TIME")
    private Instant MODIFY_TIME;

}