package com.vietcombank.service.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.UUID;

@Entity
@Table(name = "LOAN_INFO")
public class LoanInfoEntity {

    @Id
    @Column(name = "LOAN_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int LOAN_ID;

    @Getter
    @Setter
    @Column(name = "CUSTOMER_ID", nullable = false)
    private int CUSTOMER_ID;

    @Column(name = "REQUEST_AMT", nullable = false)
    private double  REQUEST_AMT;

    @Column(name = "TERM_IN_MONTH", nullable = false)
    private int TERM_IN_MONTH;

    public int getLOAN_ID() {
        return LOAN_ID;
    }

    public void setLOAN_ID(int LOAN_ID) {
        this.LOAN_ID = LOAN_ID;
    }

//    public int getCUSTOMER_ID() {
//        return CUSTOMER_ID;
//    }
//
//    public void setCUSTOMER_ID(int CUSTOMER_ID) {
//        this.CUSTOMER_ID = CUSTOMER_ID;
//    }

    public double getREQUEST_AMT() {
        return REQUEST_AMT;
    }

    public void setREQUEST_AMT(double REQUEST_AMT) {
        this.REQUEST_AMT = REQUEST_AMT;
    }

    public int getTERM_IN_MONTH() {
        return TERM_IN_MONTH;
    }

    public void setTERM_IN_MONTH(int TERM_IN_MONTH) {
        this.TERM_IN_MONTH = TERM_IN_MONTH;
    }

    public double getAPPROVE_AMT() {
        return APPROVE_AMT;
    }

    public void setAPPROVE_AMT(double APPROVE_AMT) {
        this.APPROVE_AMT = APPROVE_AMT;
    }

    @Column(name = "APPROVE_AMT")
    private double  APPROVE_AMT;

    @Column(name = "CIC_CHECK")
    private String CIC_CHECK;

    public String getCIC_CHECK() {
        return CIC_CHECK;
    }

    public void setCIC_CHECK(String product) {
        this.CIC_CHECK = product;
    }

}