package com.vietcombank.service.controller;

import com.vietcombank.service.entity.LoanInfoEntity;
import com.vietcombank.service.service.LoanInfoService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/restapi")
public class LoanInfoController {
    @Autowired
    private LoanInfoService loanInfoService;

    @GetMapping(value = "/v1/loaninfos", produces = "application/json")
    @Operation(summary = "Retrieve All Loan Info")
    public ResponseEntity<?> getListLoanInfo() {
        log.info("Lay tat cac khoan loan info");
        return ResponseEntity.ok(loanInfoService.retrieveAll());
    }


    @GetMapping("/loaninfo/{id}")
    private LoanInfoEntity getLoan(@PathVariable("id") int id)
    {
        return loanInfoService.getLoanById(id);
    }
    @DeleteMapping("/loaninfo/{id}")
    private void deleteLoanInfo(@PathVariable("id") int id)
    {
        loanInfoService.delete(id);
    }
    @PostMapping("/loaninfo")
    private int saveLoanInfo(@RequestBody LoanInfoEntity loaninfo)
    {
        loanInfoService.saveOrUpdate(loaninfo);
        return loaninfo.getLOAN_ID();
    }
    @PutMapping("/loaninfo")
    private LoanInfoEntity update(@RequestBody LoanInfoEntity loaninfo)
    {
        loanInfoService.saveOrUpdate(loaninfo);
        return loaninfo;
    }


}
