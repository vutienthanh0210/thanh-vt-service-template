package com.vietcombank.service.controller;

import com.vietcombank.service.entity.LoanInfoFlowEntity;
import com.vietcombank.service.service.LoanInfoFlowService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/restapi")
public class LoanInfoFlowController {
    @Autowired
    private LoanInfoFlowService loanInfoFlowService;

    @GetMapping(value = "/v1/listloanflow", produces = "application/json")
    @Operation(summary = "Retrieve All Loan Flow")
    public ResponseEntity<?> getListLoanFlow() {
        log.info("Lay tat cac khoan loan flow");
        return ResponseEntity.ok(loanInfoFlowService.retrieveAll());
    }


    @GetMapping("/loanflow/{id}")
    private LoanInfoFlowEntity getLoan(@PathVariable("id") int id)
    {
        return loanInfoFlowService.getLoanFlowById(id);
    }
    @DeleteMapping("/loanflow/{id}")
    private void deleteLoanInfo(@PathVariable("id") int id)
    {
        loanInfoFlowService.delete(id);
    }
    @PostMapping("/loanflow")
    private int saveLoanInfo(@RequestBody LoanInfoFlowEntity loaninfo)
    {
        loanInfoFlowService.saveOrUpdate(loaninfo);
        return loaninfo.getID();
    }
    @PutMapping("/loanflow")
    private LoanInfoFlowEntity update(@RequestBody LoanInfoFlowEntity loaninfo)
    {
        loanInfoFlowService.saveOrUpdate(loaninfo);
        return loaninfo;
    }


}
