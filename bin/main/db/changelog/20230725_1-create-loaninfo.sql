--liquibase formatted sql
--changeset microservice_class:20230725_1

CREATE
EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE LOAN_INFO
    {
    loan_id SERIAL PRIMARY KEY,
    customer_id int not null,
    request_amt DOUBLE PRECISION,
    TERM_IN_MONTH int not null,
    CIC_CHECK  VARCHAR ( 50 ),
    APPROVE_AMT DOUBLE PRECISION,
    };