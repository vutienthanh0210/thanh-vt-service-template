--liquibase formatted sql
--changeset microservice_class:20230725_1

CREATE
EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE LOAN_INFO
(
    LOAN_ID       SERIAL PRIMARY KEY,
    CUSTOMER_ID   int not null,
    REQUEST_AMT   DOUBLE PRECISION,
    TERM_IN_MONTH int not null,
    CIC_CHECK     VARCHAR(50),
    APPROVE_AMT   DOUBLE PRECISION
);

CREATE TABLE LOAN_INFO_FLOW
(
        ID SERIAL PRIMARY KEY,
        LOAN_ID int not null,
        STATUS varchar(50),
        MODIFY_USER varchar(50),
        MODIFY_TIME timestamp
);